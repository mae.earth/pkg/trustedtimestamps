/* mae.earth/pkg/trustedtimestamps/funcs.go
 * mae 12017
 */
package trustedtimestamps

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/rand"
	"time"
)

/* CreateTimestamp -- TODO: adds an options structure for setting hash functions etc */
func CreateTimestamp(datahash []byte, source string, prv *ecdsa.PrivateKey, hashfunc HashFunc) (*Timestamp, error) {

	if len(datahash) == 0 {
		return nil, ErrEmptyDataHash
	}

	if prv == nil {
		return nil, ErrInvalidPrivateKey
	}

	if hashfunc == nil {
		return nil, ErrInvalidHashFunc
	}

	now := time.Now().UTC()

	fnger, err := Fingerprint(&prv.PublicKey)
	if err != nil {
		return nil, err
	}

	/* timestamp */
	timestamp := now.Format(TimestampFormat)

	/* hash the data + timestamp */
	buf := bytes.NewBuffer(nil)
	buf.Write(datahash)
	buf.Write(Delimiter)
	buf.Write([]byte(timestamp))

	hash := hashfunc(buf.Bytes())

	buf = bytes.NewBuffer(nil)
	buf.Write([]byte(source))
	buf.Write(Delimiter)
	buf.Write(hash)
	buf.Write(Delimiter)
	buf.Write([]byte(timestamp))

	/* now sign the data */
	R, S, err := ecdsa.Sign(rand.Reader, prv, buf.Bytes())
	if err != nil {
		return nil, err
	}

	sig := bytes.NewBuffer(nil)
	sig.Write(R.Bytes())
	sig.Write(S.Bytes())

	sig_b := sig.Bytes()
	/* now create timestamp :- */

	ts := &Timestamp{Source: source, Timestamp: now}
	ts.Headers = make(map[string]string, 0)
	ts.Headers["hash"] = "sha1;data-hash+timestamp;delimited"
	ts.Headers["verify"] = "source+hash+timestamp;delimited"

	ts.DataHash = make([]byte, len(datahash))
	copy(ts.DataHash, datahash)
	ts.Hash = make([]byte, len(hash))
	copy(ts.Hash, hash)
	ts.Signature = make([]byte, len(sig_b))
	copy(ts.Signature, sig_b)

	for i := 0; i < 20; i++ {
		ts.Fingerprint[i] = fnger[i]
	}

	return ts, nil
}
