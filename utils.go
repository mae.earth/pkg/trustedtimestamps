/* mae.earth/pkg/trustedtimestamps/utils.go
 * mae 12017
 */
package trustedtimestamps

import (
	"crypto/sha1"
	"fmt"
	"strings"
)

/* shorten */
func shorten(in string) string {
	if len(in) < 8 {
		return in
	}

	return in[:8] + "..."
}

/* fingerprint2string */
func fingerprint2string(pub [20]byte) string {

	return fmt.Sprintf("%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x",
		pub[0], pub[1], pub[2], pub[3], pub[4], pub[5], pub[6], pub[7], pub[8], pub[9], pub[10],
		pub[11], pub[12], pub[13], pub[14], pub[15], pub[16], pub[17], pub[18], pub[19])
}

func tohashfunction(hash string) HashFunc {

	var f HashFunc

	switch strings.ToLower(hash) {
	case "sha1":

		f = func(in []byte) []byte {

			s := sha1.New()
			s.Write(in)
			return s.Sum(nil)
		}

		break
	}

	return f
}
