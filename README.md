# Trusted Timestamps
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/mae.earth/pkg/trustedtimestamps)](https://goreportcard.com/report/gitlab.com/mae.earth/pkg/trustedtimestamps)

Implementation of Trusted Timestamps in golang. 

```go 
package main

import (
	"fmt"
	tts "gitlab.com/mae.earth/pkg/trustedtimestamps"
	"io/ioutil"
	"os"
)

func main() {

	/* setup server */

	config := &tts.Configuration{Domain: "time.example.org",
		Hash: "sha1",
	}

	server, err := tts.NewServer(config)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error creating new tts server -- %v", err)
		os.Exit(1)
	}

	if err := server.GenerateKey(); err != nil {
		fmt.Fprintf(os.Stderr, "error generating server key -- %v", err)
		os.Exit(1)
	}

	/* export banner from server */

	banner, err := server.ExportBanner()
	if err != nil {
		fmt.Fprintf(os.Stderr, "error exporting banner -- %v", err)
		os.Exit(1)
	}

	if err := ioutil.WriteFile("banner.pem", banner, 0664); err != nil {
		fmt.Fprintf(os.Stderr, "error writing %q to disk -- %v", "banner.pem", err)
		os.Exit(1)
	}

	/* setup client */

	config = &tts.Configuration{Domain: "client.example.org",
		Hash: "sha1",
	}

	client, err := tts.NewClient(config)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error creating new tts client -- %v", err)
		os.Exit(1)
	}

	/* add server (from banner) to client */

	if err := client.AddToKeychain(banner); err != nil {
		fmt.Fprintf(os.Stderr, "error adding banner to keychain -- %v", err)
		os.Exit(1)
	}

	/* create timestamp, prepared from the client and generated at the server */

	timestamp, err := server.Timestamp(client.Prepare([]byte("payload")))
	if err != nil {
		fmt.Fprintf(os.Stderr, "error timestamping client data -- %v", err)
		os.Exit(1)
	}

	/* armour timestamp for writing to file etc.. */

	armour := tts.ArmourTimestamps([]*tts.Timestamp{timestamp})

	if err := ioutil.WriteFile("timestamp.pem", armour[0], 0664); err != nil {
		fmt.Fprintf(os.Stderr, "error writing %q to disk -- %v", "timestamp.pem", err)
		os.Exit(1)
	}

	/* dearmout timestamp from file */

	timestamps, err := tts.DearmourTimestamps(armour)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error dearmour timestamp -- %v", err)
		os.Exit(1)
	}

	/* verify timestamp at the client */

	ok, err := client.Verify(timestamps[0])
	if err != nil {
		fmt.Fprintf(os.Stderr, "error verifying timestamp with client -- %v", err)
		os.Exit(1)
	}

	if !ok {
		fmt.Fprintf(os.Stderr, "bad timestamp")
		os.Exit(2)
	}

}
```

banner.pem

```bash
-----BEGIN TRUSTED TIMESTAMP BANNER-----
fingerprint: b:10:a5:a7:cb:68:fc:91:6f:75:de:fa:9d:1a:10:61:b7:cc:d6:fa
key-type: ecdsa
source: time.example.org
timestamp: Wed, 24 Jan 2018 13:36:26 +0000

ME4wEAYHKoZIzj0CAQYFK4EEACEDOgAEa/WUtnrhMc1pt9kMVp2glb5YFI1s4PV7
7rMovJMOT2T/Egjep14yNwUUHqDQObvSTJuQYPTVsAw=
-----END TRUSTED TIMESTAMP BANNER-----
```

timestamp.pem

```bash
-----BEGIN TRUSTED TIMESTAMP-----
data-hash: f07e5a815613c5abeddc4b682247a4c42d8a95df
fingerprint: b:10:a5:a7:cb:68:fc:91:6f:75:de:fa:9d:1a:10:61:b7:cc:d6:fa
hash: sha1;data-hash+timestamp;delimited;edb189c59930ffba5be24a5539703c962cd17239
source: time.example.org
timestamp: Wed, 24 Jan 2018 13:36:26 +0000
verify: source+hash+timestamp;delimited

/n/8j64+zhEUKI5+JAx3azKsjrZYXv0q/SuBGhWbQ6fMnxUzbmasdBbujIuhwf3s
4eAcOFxnP54=
-----END TRUSTED TIMESTAMP-----
```





