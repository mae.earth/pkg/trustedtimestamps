/* mae.earth/pkg/trustedtimestamps/timestamps_test.go
 * mae 12017
 */
package trustedtimestamps

import (
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"time"
)

const test_private_key = `
-----BEGIN EC PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: AES-256-CBC,f49a599ba6195aa47d698375ccf4b658

OmwOqPEGYz/n3w9bvysRopAo2C6Dtb5AfOqQaZmj715XH4nTQssNuB+lxgghPdR7
0GQusslv07EQZtfB+eQ7Bmfh/xcB3lsfoVvjhfHk3/IOKKRavSfRVKYS7y2i1I9L
/BlYEi1B0yLBJoYx6wboDQ==
-----END EC PRIVATE KEY-----
`

const test_source = "testing.trustedtimestamps"

func Test_BasicFunctions(t *testing.T) {

	Convey("Basic Functions", t, func() {

		private, err := openArmouredECDSAKey([]byte(test_private_key), []byte("password"))
		So(err, ShouldBeNil)
		So(private, ShouldNotBeNil)

		public := &private.PublicKey

		/* test the timestamp function */
		Convey("Timestamp", func() {

			then := time.Now()

			timestamp, err := NewTimestamp([]byte("TEST·DATA·HASH"), test_source, private)
			So(err, ShouldBeNil)
			So(timestamp, ShouldNotBeNil)

			fmt.Printf("timestamp = %s\n", timestamp)

			Convey("does the timestamp fall between two known times", func() {

				So(timestamp.Timestamp.After(then), ShouldBeTrue)
				So(timestamp.Timestamp.Before(time.Now()), ShouldBeTrue)
			})

			Convey("does data-hash match", func() {

				So(string(timestamp.DataHash), ShouldEqual, "TEST·DATA·HASH")
			})

			Convey("does source match", func() {

				So(timestamp.Source, ShouldEqual, test_source)
			})

			Convey("does signature match", func() {

				So(EqualsFingerprint(public, timestamp.Fingerprint), ShouldBeTrue)
			})

			Convey("verify timestamp", func() {

				So(VerifyTimestamp(timestamp, public), ShouldBeTrue)
			})
		})

		Convey("Armour", func() {

			t0 := time.Now()

			timestamp, err := NewTimestamp([]byte("TEST·DATA·HASH"), test_source, private)
			So(err, ShouldBeNil)
			So(timestamp, ShouldNotBeNil)

			Convey("armour a single timestamp", func() {

				armoured := ArmourTimestamps([]*Timestamp{timestamp})
				So(armoured, ShouldNotBeNil)
				So(len(armoured), ShouldEqual, 1)

				fmt.Printf("armoured = %s\nsize = %dbytes\n", armoured[0], len(armoured[0]))

				compressed, err := CompressArmoured(armoured)
				So(err, ShouldBeNil)

				fmt.Printf("compressed = %dbytes\n", len(compressed[0]))
				fmt.Printf("-- took %v\n", time.Since(t0))

				/* attempt a dearmour */
				dearmoured, err := DearmourTimestamps(armoured)
				So(err, ShouldBeNil)
				So(dearmoured, ShouldNotBeNil)
				So(len(dearmoured), ShouldEqual, 1)

				fmt.Printf("dearmoured = %s\n", dearmoured[0])

				So(dearmoured[0].Equals(timestamp), ShouldBeTrue)
			})

			Convey("sending nil timestamps to armour", func() {

				So(ArmourTimestamps(nil), ShouldBeNil)
			})

		})

	})
}

func Test_FuzzCrashers(t *testing.T) {
	Convey("Fuzz Crashers",t,func() {
		Convey("da39a3ee5e6b4b0d3255bfef95601890afd80709",func() {
			stamps,err := DearmourTimestamps([][]byte{[]byte(" ")}) 
			So(stamps,ShouldBeNil)
			So(err,ShouldNotBeNil)
		})
	})
}


func Benchmark_Timestamp(b *testing.B) {

	b.StopTimer()

	private, err := openArmouredECDSAKey([]byte(test_private_key), []byte("password"))
	if err != nil {
		panic(err)
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		_, err := NewTimestamp([]byte("TEST·DATA·HASH"), test_source, private)
		if err != nil {
			panic(err)
		}
	}
}

func Benchmark_VerifyTimestamp(b *testing.B) {

	b.StopTimer()

	private, err := openArmouredECDSAKey([]byte(test_private_key), []byte("password"))
	if err != nil {
		panic(err)
	}

	timestamp, err := NewTimestamp([]byte("TEST·DATA·HASH"), test_source, private)
	if err != nil {
		panic(err)
	}

	public := &private.PublicKey

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		VerifyTimestamp(timestamp, public)
	}
}

func Benchmark_Armour(b *testing.B) {

	b.StopTimer()

	private, err := openArmouredECDSAKey([]byte(test_private_key), []byte("password"))
	if err != nil {
		panic(err)
	}

	timestamp, err := NewTimestamp([]byte("TEST·DATA·HASH"), test_source, private)
	if err != nil {
		panic(err)
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		ArmourTimestamps([]*Timestamp{timestamp})
	}
}

func Benchmark_Compress(b *testing.B) {

	b.StopTimer()

	private, err := openArmouredECDSAKey([]byte(test_private_key), []byte("password"))
	if err != nil {
		panic(err)
	}

	timestamp, err := NewTimestamp([]byte("TEST·DATA·HASH"), test_source, private)
	if err != nil {
		panic(err)
	}

	armour := ArmourTimestamps([]*Timestamp{timestamp})

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		CompressArmoured(armour)
	}
}

func Benchmark_Uncompress(b *testing.B) {

	b.StopTimer()

	private, err := openArmouredECDSAKey([]byte(test_private_key), []byte("password"))
	if err != nil {
		panic(err)
	}

	timestamp, err := NewTimestamp([]byte("TEST·DATA·HASH"), test_source, private)
	if err != nil {
		panic(err)
	}

	armour := ArmourTimestamps([]*Timestamp{timestamp})

	compressed, err := CompressArmoured(armour)
	if err != nil {
		panic(err)
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		UncompressArmoured(compressed)
	}
}

func Benchmark_Unarmour(b *testing.B) {

	b.StopTimer()

	private, err := openArmouredECDSAKey([]byte(test_private_key), []byte("password"))
	if err != nil {
		panic(err)
	}

	timestamp, err := NewTimestamp([]byte("TEST·DATA·HASH"), test_source, private)
	if err != nil {
		panic(err)
	}

	armour := ArmourTimestamps([]*Timestamp{timestamp})

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		DearmourTimestamps(armour)
	}
}

/* Benchmark_TimestampArmour */
func Benchmark_TimestampArmourCompress(b *testing.B) {

	b.StopTimer()

	private, err := openArmouredECDSAKey([]byte(test_private_key), []byte("password"))
	if err != nil {
		panic(err)
	}

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		TimestampArmourCompress([]byte("TEST·DATA·HASH"), test_source, private)
	}
}

func init() {

	/*
		armoured,err := generateAndArmourECDSAKey([]byte("password"))
		if err != nil {
			panic(err)
		}

		fmt.Printf("%s\n",string(armoured))
	*/
}
