/* mae.earth/pkg/trustedtimestamps/definitions.go
 * mae 12017
 */
package trustedtimestamps

import (
	"encoding/hex"
	"errors"
	"fmt"
	"time"
)

var (
	ErrNoKey                = errors.New("No Key")
	ErrInvalidBanner        = errors.New("Invalid Banner")
	ErrBlockIsEncrypted     = errors.New("Block is Encrypted")
	ErrInvalidBlockType     = errors.New("Invalid block type")
	ErrBadKeyType           = errors.New("Bad Key Type")
	ErrBadKey               = errors.New("Bad Key")
	ErrAlreadyPresent       = errors.New("Already Present")
	ErrInvalidTimestamp     = errors.New("Invalid Timestamp")
	ErrEmptyKeychain        = errors.New("Empty Keychain")
	ErrUnknownSource        = errors.New("Unknown Source")
	ErrRequireConfiguration = errors.New("Require Configuration")
	ErrInvalidHashFunc      = errors.New("Invalid Hash Function")
)

type Stat struct {
	Requests int
}

/* Timestamp */
type Timestamp struct {
	Headers map[string]string /* comma seperated tags, hash = sha1,delimited */

	Source    string
	DataHash  []byte
	Hash      []byte
	Timestamp time.Time

	Fingerprint [20]byte
	Signature   []byte

	/* TODO: need to add verifying headers for autoconfiguration */
}

func (ts *Timestamp) String() string {
	return fmt.Sprintf("source=%s, data-hash=%s, hash=%s, timestamp=%s",
		ts.Source, shorten(hex.EncodeToString(ts.DataHash)),
		shorten(hex.EncodeToString(ts.Hash)),
		ts.Timestamp.Format(TimestampFormat))
}

func (ts *Timestamp) Armour() []byte {
	return ArmourTimestamps([]*Timestamp{ts})[0]
}

func (ts *Timestamp) Equals(o *Timestamp) bool {
	if o == nil {
		return false
	}

	if o.Source != ts.Source {
		return false
	}

	if o.Timestamp.Equal(ts.Timestamp) {
		return false
	}

	if len(o.DataHash) != len(ts.DataHash) {
		return false
	}

	for i := 0; i < len(o.DataHash); i++ {
		if o.DataHash[i] != ts.DataHash[i] {
			return false
		}
	}

	if len(o.Hash) != len(ts.Hash) {
		return false
	}

	for i := 0; i < len(o.Hash); i++ {
		if o.Hash[i] != ts.Hash[i] {
			return false
		}
	}

	if len(o.Signature) != len(ts.Signature) {
		return false
	}

	for i := 0; i < len(o.Signature); i++ {
		if o.Signature[i] != ts.Signature[i] {
			return false
		}
	}

	for i := 0; i < 20; i++ {
		if o.Fingerprint[i] != ts.Fingerprint[i] {
			return false
		}
	}

	return true
}

type Configuration struct {

	/* TODO: add in the hash functions etc */
	Domain string

	Hash string
}

type HashFunc func([]byte) []byte
