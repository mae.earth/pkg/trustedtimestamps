/* mae.earth/pkg/trustedtimestamps/server_test.go
 * mae 12017
 */
package trustedtimestamps

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"

	"fmt"
)

func Test_Server(t *testing.T) {

	Convey("Server", t, func() {

		Convey("invalid configuration", func() {

			server, err := NewServer(nil)
			So(err, ShouldEqual, ErrRequireConfiguration)
			So(server, ShouldBeNil)
		})

		Convey("bad domain name", func() {

			server, err := NewServer(&Configuration{})
			So(err, ShouldNotBeNil)
			So(server, ShouldBeNil)
		})

		server, err := NewServer(&Configuration{Domain: "testing.trustedtimestamps", Hash: "sha1"})
		So(err, ShouldBeNil)
		So(server, ShouldNotBeNil)

		Convey("should not have a key", func() {

			So(server.HaveKey(), ShouldBeFalse)
		})

		Convey("should not be able to generate a timestamp", func() {

			timestamp, err := server.Timestamp([]byte("TEST·DATA·HASH"))
			So(err, ShouldEqual, ErrNoKey)
			So(timestamp, ShouldBeNil)
		})

		Convey("generate a key then a timestamp", func() {

			So(server.GenerateKey(), ShouldBeNil)

			timestamp, err := server.Timestamp([]byte("TEST·DATA·HASH"))
			So(err, ShouldBeNil)
			So(timestamp, ShouldNotBeNil)

			banner, err := server.ExportBanner()
			So(err, ShouldBeNil)
			So(banner, ShouldNotBeNil)

			fmt.Printf("%s\n", string(banner))
		})

	})
}

func Test_ServerClient(t *testing.T) {

	Convey("Server --> banner --> Client", t, func() {

		server, err := NewServer(&Configuration{Domain: "testing.trustedtimestamps", Hash: "sha1"})
		So(err, ShouldBeNil)
		So(server, ShouldNotBeNil)

		So(server.GenerateKey(), ShouldBeNil)

		banner, err := server.ExportBanner()
		So(err, ShouldBeNil)
		So(banner, ShouldNotBeNil)

		client, err := NewClient(&Configuration{Domain: "client.trustedtimestamps", Hash: "sha1"})
		So(err, ShouldBeNil)
		So(client, ShouldNotBeNil)

		So(client.AddToKeychain(banner), ShouldBeNil)

		Convey("should error at badly formed banner", func() {
			So(client.AddToKeychain([]byte("Some fake banner")), ShouldEqual, ErrInvalidBanner)
		})

		Convey("should error at nil banner", func() {
			So(client.AddToKeychain(nil), ShouldEqual, ErrInvalidBanner)
		})

		Convey("should not be able to duplicate banners in keychain", func() {
			So(client.AddToKeychain(banner), ShouldEqual, ErrAlreadyPresent)
		})

		Convey("generate timestamp, then verify", func() {
			timestamp, err := server.Timestamp(client.Prepare([]byte("some·data·to·hash")))
			So(err, ShouldBeNil)
			So(timestamp, ShouldNotBeNil)

			ok, err := client.Verify(timestamp)
			So(err, ShouldBeNil)
			So(ok, ShouldBeTrue)
		})

	})
}
