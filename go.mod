module mae.earth/pkg/trustedtimestamps

go 1.14

require (
	github.com/smartystreets/goconvey v1.6.4
	gitlab.com/mae.earth/pkg/trustedtimestamps v0.0.0-20181023150738-77e33432597d // indirect
)
