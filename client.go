/* mae.earth/pkg/trustedtimestamps/client.go
 * mae 12017
 */
package trustedtimestamps

import (
	"crypto/ecdsa"
	"crypto/x509"

	"bytes"
	"encoding/pem"
	"fmt"
	"math/big"
	"strings"
	"sync"
	"time"
)

type ServerBanner struct {
	Source      string
	Fingerprint [20]byte
	PublicKey   *ecdsa.PublicKey
}

/* Client */
type Client struct {
	mux sync.RWMutex

	requests int

	/* hold known server public keys here */
	keychain []*ServerBanner
	hashfunc HashFunc

	config *Configuration
}

/* Stat */
func (client *Client) Stat() Stat {
	client.mux.RLock()
	defer client.mux.RUnlock()

	return Stat{Requests: client.requests}
}

/* TODO: replace source with server options structure */
func (client *Client) AddToKeychain(data []byte) error {
	client.mux.Lock()
	defer client.mux.Unlock()

	/* TODO: we must be MORE strict with the banner formats */

	block, _ := pem.Decode(data)
	if block == nil {
		return ErrInvalidBanner
	}

	if block.Type != BannerType {
		return ErrInvalidBlockType
	}

	if x509.IsEncryptedPEMBlock(block) {
		return ErrBlockIsEncrypted
	}

	/* key-type */
	if t, exists := block.Headers["key-type"]; exists {

		if strings.ToLower(t) != "ecdsa" {
			return ErrBadKeyType
		}
	}

	/* check if there is a timestamp attached with the headers */
	if t, exists := block.Headers["timestamp"]; exists {

		timestamp, err := time.Parse(TimestampFormat, t)
		if err != nil {
			return err
		}

		/* if timestamp is AFTER the current time then invalid */
		if timestamp.After(time.Now()) {
			return ErrInvalidTimestamp
		}
	}

	/* source */
	if t, exists := block.Headers["source"]; exists {
		/* TODO: check valid domain name */
		if len(t) == 0 {
			return fmt.Errorf("\"source\" not defined in banner")
		}
	} else {
		return fmt.Errorf("\"source\" not defined in banner")
	}

	/* from this point assuming EC Public key */

	key, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return err
	}

	var pub *ecdsa.PublicKey

	switch key.(type) {
	case *ecdsa.PublicKey:
		pub = key.(*ecdsa.PublicKey)
		break
	default:

		return ErrBadKey
		break
	}

	banner := &ServerBanner{Source: block.Headers["source"]}

	fingerprint, err := Fingerprint(pub)
	if err != nil {
		return err
	}

	for i := 0; i < 20; i++ {
		banner.Fingerprint[i] = fingerprint[i]
	}

	banner.PublicKey = pub

	for _, banner_in_chain := range client.keychain {
		match := true
		for i := 0; i < 20; i++ {
			if banner_in_chain.Fingerprint[i] != fingerprint[i] {
				match = false
				break
			}
		}
		if match {
			return ErrAlreadyPresent
		}
	}

	client.keychain = append(client.keychain, banner)

	return nil
}

/* RemoveFromKeychain */
func (client *Client) RemoveFromKeychain(fingerprint [20]byte) error {
	client.mux.Lock()
	defer client.mux.Unlock()

	client.requests++

	if len(client.keychain) == 0 {
		return nil
	}

	found := -1
	for i, banner := range client.keychain {
		match := true
		for i := 0; i < 20; i++ {
			if banner.Fingerprint[i] != fingerprint[i] {
				match = false
				break
			}
		}
		if match {
			found = i
			break
		}
	}

	if found > -1 {

		n := make([]*ServerBanner, 0)
		if found > 0 {
			n = append(n, client.keychain[:found]...)
		}
		if found < len(client.keychain) {
			n = append(n, client.keychain[found:]...)
		}

		client.keychain = n
	}

	return nil
}

/* KnownFingerprints */
func (client *Client) KnownFingerprints() []string {
	client.mux.RLock()
	defer client.mux.RUnlock()

	out := make([]string,len(client.keychain))
	
	for i,banner := range client.keychain {
		out[i] = fingerprint2string(banner.Fingerprint)
	}

	return out
}


/* Prepare */
func (client *Client) Prepare(data []byte) []byte {
	client.mux.RLock()
	defer client.mux.RUnlock()

	return client.hashfunc(data)
}
/* Verify */
func (client *Client) Verify(timestamp *Timestamp) (bool, error) {
	client.mux.RLock()
	defer client.mux.RUnlock()

	if timestamp == nil {
		return false, ErrInvalidTimestamp
	}

	if len(client.keychain) == 0 {
		return false, ErrEmptyKeychain
	}

	find := func(fingerprint [20]byte) *ServerBanner {
		for _, banner := range client.keychain {
			match := true
			for i := 0; i < 20; i++ {
				if fingerprint[i] != banner.Fingerprint[i] {
					match = false
					break
				}
			}
			if match {
				return banner
			}
		}
		return nil
	}

	banner := find(timestamp.Fingerprint)
	if banner == nil {
		return false, ErrUnknownSource
	}

	if banner.Source != timestamp.Source {
		return false, fmt.Errorf("expecting source=\"%s\", but was \"%s\"", banner.Source, timestamp.Source)
	}

	r := big.NewInt(0)
	r.SetBytes(timestamp.Signature[:len(timestamp.Signature)/2])

	s := big.NewInt(0)
	s.SetBytes(timestamp.Signature[len(timestamp.Signature)/2:])

	/* FIXME: generate own hash */
	buf := bytes.NewBuffer(nil)
	buf.Write([]byte(timestamp.Source))
	buf.Write(Delimiter)
	buf.Write(timestamp.Hash)
	buf.Write(Delimiter)
	buf.Write([]byte(timestamp.Timestamp.Format(TimestampFormat)))

	return ecdsa.Verify(banner.PublicKey, buf.Bytes(), r, s), nil
}
/* NewClient */
func NewClient(conf *Configuration) (*Client, error) {
	client := &Client{}
	client.keychain = make([]*ServerBanner, 0)

	client.config = &Configuration{}

	if conf == nil {
		return nil, ErrRequireConfiguration
	}

	client.config.Hash = conf.Hash

	f := tohashfunction(conf.Hash)
	if f == nil {
		return nil, fmt.Errorf("Unknown hash function")
	}

	client.hashfunc = f

	/* TODO: parse and set configuration */
	if len(conf.Domain) == 0 {
		/* TODO: do a proper check of the domain name */
		return nil, fmt.Errorf("Domain not set")
	}

	client.config.Domain = conf.Domain

	return client, nil
}
