/* mae.earth/pkg/trustedtimestamps/server.go
 * mae 12017
 */
package trustedtimestamps

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"

	"fmt"
	"sync"
	"time"
)

const (
	BannerType = "TRUSTED TIMESTAMP BANNER"
)

/* Server */
type Server struct {
	mux sync.RWMutex

	requests int

	/* hold private keys here */
	key *ecdsa.PrivateKey /* FIXME, needs to be []array of keys so that the server can create new keys as needed */

	hashfunc HashFunc

	config *Configuration
}

func (server *Server) Stat() Stat {
	server.mux.RLock()
	defer server.mux.RUnlock()

	return Stat{Requests: server.requests}
}

/* Timstamp */
func (server *Server) Timestamp(datahash []byte) (*Timestamp, error) {
	defer func() { server.mux.Lock(); server.requests++; server.mux.Unlock() }() /* FIXME, this is messy */
	server.mux.RLock()
	defer server.mux.RUnlock()

	if server.key == nil {
		return nil, ErrNoKey
	}

	return CreateTimestamp(datahash, server.config.Domain, server.key, server.hashfunc)
}

/* Key */
func (server *Server) Key() *ecdsa.PrivateKey {
	server.mux.RLock()
	defer server.mux.RUnlock()

	return server.key
}

/* HaveKey */
func (server *Server) HaveKey() bool {
	server.mux.RLock()
	defer server.mux.RUnlock()

	return (server.key != nil)
}

/* UseKey */
func (server *Server) UseKey(prv *ecdsa.PrivateKey) error {
	server.mux.Lock()
	defer server.mux.Unlock()

	if prv == nil {
		return ErrBadKey
	}

	server.key = prv

	return nil
}

/* ExportBanner */
func (server *Server) ExportBanner() ([]byte, error) {
	server.mux.RLock()
	defer server.mux.RUnlock()

	if server.key == nil {
		return nil, ErrNoKey
	}

	key, err := x509.MarshalPKIXPublicKey(server.key.Public())
	if err != nil {
		return nil, err
	}

	fingerprint, err := Fingerprint(&server.key.PublicKey)
	if err != nil {
		return nil, err
	}

	headers := make(map[string]string, 0)

	headers["source"] = server.config.Domain
	headers["fingerprint"] = fingerprint2string(fingerprint)
	headers["key-type"] = "ecdsa"
	headers["timestamp"] = time.Now().UTC().Format(TimestampFormat)

	return pem.EncodeToMemory(&pem.Block{Type: BannerType, Bytes: key, Headers: headers}), nil
}

/* GenerateKey */
func (server *Server) GenerateKey() error {
	server.mux.Lock()
	defer server.mux.Unlock()

	/* TODO, need configuration options here */
	p, err := ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	if err != nil {
		return err
	}

	server.key = p

	return nil
}

/* NewServer */
func NewServer(conf *Configuration) (*Server, error) {
	server := &Server{}
	server.config = &Configuration{}

	if conf == nil {
		return nil, ErrRequireConfiguration
	}

	server.config.Hash = conf.Hash

	f := tohashfunction(conf.Hash)
	if f == nil {
		return nil, fmt.Errorf("Unknown hash function")
	}

	server.hashfunc = f

	/* do some checking : */
	if len(conf.Domain) == 0 {
		/* TODO: do a proper check of the domain name */
		return nil, fmt.Errorf("Domain not set")
	}

	server.config.Domain = conf.Domain

	return server, nil
}

/* TODO: below is used in a number of functions for tests, but could be made general */

/* generateAndArmourECDSAKey, used for testing */
func generateAndArmourECDSAKey(password []byte) ([]byte, error) {

	p, err := ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	if err != nil {
		return nil, err
	}

	p_b, err := x509.MarshalECPrivateKey(p)
	if err != nil {
		return nil, err
	}

	block, err := x509.EncryptPEMBlock(rand.Reader, "EC PRIVATE KEY", p_b, []byte(password), x509.PEMCipherAES256)
	if err != nil {
		return nil, err
	}

	return pem.EncodeToMemory(block), nil
}

func openArmouredECDSAKey(armour, password []byte) (*ecdsa.PrivateKey, error) {

	block, _ := pem.Decode(armour)
	if block == nil {
		return nil, fmt.Errorf("Invalid Armoured Key")
	}

	var data []byte

	if x509.IsEncryptedPEMBlock(block) {

		d, err := x509.DecryptPEMBlock(block, password)
		if err != nil {
			return nil, err
		}

		data = make([]byte, len(d))
		copy(data, d)
	} else {

		data = make([]byte, len(block.Bytes))
		copy(data, block.Bytes)
	}

	private, err := x509.ParseECPrivateKey(data)
	if err != nil {
		return nil, err
	}

	return private, nil
}
