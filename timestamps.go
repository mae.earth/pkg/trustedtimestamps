/* mae.earth/pkg/trustedtimestamps/timestamps.go
 * mae 12017
 */
//Package trustedtimestamps
package trustedtimestamps

import (
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha1"

	"bytes"
	"compress/flate"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"math/big"
	"strings"
	"time"
)

var (
	ErrEmptyDataHash     = errors.New("Empty Data Hash")
	ErrInvalidPrivateKey = errors.New("Invalid Private Key")
	ErrInvalidPublicKey  = errors.New("Invalid Public Key")
)

const (
	TimestampFormat = time.RFC1123Z
)

var (
	Delimiter = []byte(":")
)

/* Fingerprint */
func Fingerprint(pub *ecdsa.PublicKey) ([20]byte, error) {
	if pub == nil {
		return [20]byte{}, ErrInvalidPublicKey
	}

	buf := bytes.NewBuffer(nil)
	buf.Write(pub.X.Bytes())
	buf.Write(pub.Y.Bytes())

	s := sha1.New()
	s.Write(buf.Bytes())

	h := s.Sum(nil)
	out := [20]byte{}
	for i := 0; i < 20; i++ {
		out[i] = h[i]
	}
	return out, nil
}

/* EqualsFingerprint */
func EqualsFingerprint(pub *ecdsa.PublicKey, fnger [20]byte) bool {
	fnger2, err := Fingerprint(pub)
	if err != nil {
		return false
	}

	for i := 0; i < 20; i++ {
		if fnger2[i] != fnger[i] {
			return false
		}
	}

	return true
}

/* VerifyTimestamp */
func VerifyTimestamp(timestamp *Timestamp, pub *ecdsa.PublicKey) bool {
	if timestamp == nil || pub == nil {
		return false
	}

	r := big.NewInt(0)
	r.SetBytes(timestamp.Signature[:len(timestamp.Signature)/2])

	s := big.NewInt(0)
	s.SetBytes(timestamp.Signature[len(timestamp.Signature)/2:])

	buf := bytes.NewBuffer(nil)
	buf.Write([]byte(timestamp.Source))
	buf.Write(Delimiter)
	buf.Write(timestamp.Hash)
	buf.Write(Delimiter)
	buf.Write([]byte(timestamp.Timestamp.Format(TimestampFormat)))

	return ecdsa.Verify(pub, buf.Bytes(), r, s)
}

/* FIXME: not used anymore -- see CreateTimestamp instead */
func NewTimestamp(datahash []byte, source string, prv *ecdsa.PrivateKey) (*Timestamp, error) {

	if len(datahash) == 0 {
		return nil, ErrEmptyDataHash
	}

	if prv == nil {
		return nil, ErrInvalidPrivateKey
	}

	now := time.Now().UTC()

	fnger, err := Fingerprint(&prv.PublicKey)
	if err != nil {
		return nil, err
	}

	/* timestamp */
	timestamp := now.Format(TimestampFormat)

	/* hash the data + timestamp */
	s := sha1.New()
	s.Write(datahash)
	s.Write(Delimiter)
	s.Write([]byte(timestamp))

	hash := s.Sum(nil)

	buf := bytes.NewBuffer(nil)
	buf.Write([]byte(source))
	buf.Write(Delimiter)
	buf.Write(hash)
	buf.Write(Delimiter)
	buf.Write([]byte(timestamp))

	/* now sign the data */
	R, S, err := ecdsa.Sign(rand.Reader, prv, buf.Bytes())
	if err != nil {
		return nil, err
	}

	sig := bytes.NewBuffer(nil)
	sig.Write(R.Bytes())
	sig.Write(S.Bytes())

	sig_b := sig.Bytes()
	/* now create timestamp :- */

	ts := &Timestamp{Source: source, Timestamp: now}
	ts.Headers = make(map[string]string, 0)
	ts.Headers["hash"] = "sha1;data-hash+timestamp;delimited"
	ts.Headers["verify"] = "source+hash+timestamp;delimited"

	ts.DataHash = make([]byte, len(datahash))
	copy(ts.DataHash, datahash)
	ts.Hash = make([]byte, len(hash))
	copy(ts.Hash, hash)
	ts.Signature = make([]byte, len(sig_b))
	copy(ts.Signature, sig_b)

	for i := 0; i < 20; i++ {
		ts.Fingerprint[i] = fnger[i]
	}

	return ts, nil
}

/* ArmourTimestamps */
func ArmourTimestamps(timestamps []*Timestamp) [][]byte {
	if len(timestamps) == 0 {
		return nil
	}

	out := make([][]byte, 0)

	for _, timestamp := range timestamps {

		/* TODO: all the header blocks need to be read from the timestamp header block instead of hard coded */

		headers := make(map[string]string, 0)

		if t, exists := timestamp.Headers["source"]; exists {
			headers["source"] = fmt.Sprintf("%s;%s", t, timestamp.Source)
		} else {
			headers["source"] = timestamp.Source
		}

		if t, exists := timestamp.Headers["data-hash"]; exists {
			headers["data-hash"] = fmt.Sprintf("%s;%s", t, hex.EncodeToString(timestamp.DataHash))
		} else {
			headers["data-hash"] = hex.EncodeToString(timestamp.DataHash)
		}

		if t, exists := timestamp.Headers["hash"]; exists {
			headers["hash"] = fmt.Sprintf("%s;%s", t, hex.EncodeToString(timestamp.Hash))
		} else {
			headers["hash"] = hex.EncodeToString(timestamp.Hash)
		}

		if t, exists := timestamp.Headers["timestamp"]; exists {
			headers["timestamp"] = fmt.Sprintf("%s;%s", t, timestamp.Timestamp.Format(TimestampFormat))
		} else {
			headers["timestamp"] = timestamp.Timestamp.Format(TimestampFormat)
		}

		if t, exists := timestamp.Headers["verify"]; exists {
			headers["verify"] = t
		}

		pub := timestamp.Fingerprint

		fngprint := fmt.Sprintf("%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x",
			pub[0], pub[1], pub[2], pub[3], pub[4], pub[5], pub[6], pub[7], pub[8], pub[9], pub[10],
			pub[11], pub[12], pub[13], pub[14], pub[15], pub[16], pub[17], pub[18], pub[19])

		if t, exists := timestamp.Headers["fingerprint"]; exists {
			headers["fingerprint"] = fmt.Sprintf("%s;%s", t, fngprint)
		} else {
			headers["fingerprint"] = fngprint
		}

		out = append(out, pem.EncodeToMemory(&pem.Block{Type: "TRUSTED TIMESTAMP", Bytes: timestamp.Signature, Headers: headers}))
	}

	return out
}
/* DearmourTimestamps */
func DearmourTimestamps(armours [][]byte) ([]*Timestamp, error) {
	if len(armours) == 0 {
		return nil, nil
	}

	out := make([]*Timestamp, 0)

	for _, armour := range armours {

		ban, _ := pem.Decode(armour)
		if ban == nil || ban.Type != "TRUSTED TIMESTAMP" {
			if ban == nil {
				return nil, fmt.Errorf("invalid pem block -- was nil")
			}
			return nil, fmt.Errorf("invalid pem type -- %q", ban.Type)
		}

		timestamp := &Timestamp{}

		if t, exists := ban.Headers["timestamp"]; exists {

			if at, err := time.Parse(TimestampFormat, t); err != nil {
				return nil, err
			} else {

				timestamp.Timestamp = at
			}
		}

		if t, exists := ban.Headers["source"]; exists {
			timestamp.Source = t
		}

		if t, exists := ban.Headers["data-hash"]; exists {
			if at, err := hex.DecodeString(t); err != nil {
				return nil, err
			} else {

				timestamp.DataHash = make([]byte, len(at))
				copy(timestamp.DataHash, at)
			}
		}

		if t, exists := ban.Headers["hash"]; exists {

			parts := strings.Split(t, ";")
			if len(parts) == 0 {
				return nil, fmt.Errorf("expecting parts")
			}

			/* TODO: parse the parts */
			if at, err := hex.DecodeString(parts[len(parts)-1]); err != nil {
				return nil, err
			} else {

				timestamp.Hash = make([]byte, len(at))
				copy(timestamp.Hash, at)
			}
		}

		if t, exists := ban.Headers["fingerprint"]; exists {

			fmt.Sscanf(t, "%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x", &timestamp.Fingerprint[0],
				&timestamp.Fingerprint[1], &timestamp.Fingerprint[2], &timestamp.Fingerprint[3], &timestamp.Fingerprint[4],
				&timestamp.Fingerprint[5], &timestamp.Fingerprint[6], &timestamp.Fingerprint[7], &timestamp.Fingerprint[8],
				&timestamp.Fingerprint[9], &timestamp.Fingerprint[10], &timestamp.Fingerprint[11], &timestamp.Fingerprint[12],
				&timestamp.Fingerprint[13], &timestamp.Fingerprint[14], &timestamp.Fingerprint[15], &timestamp.Fingerprint[16],
				&timestamp.Fingerprint[17], &timestamp.Fingerprint[18], &timestamp.Fingerprint[19])

		}

		timestamp.Signature = make([]byte, len(ban.Bytes))
		copy(timestamp.Signature, ban.Bytes)

		out = append(out, timestamp)
	}

	return out, nil
}

/* CompressArmoured */
func CompressArmoured(armoured [][]byte) ([][]byte, error) {

	const dict = `hash` + `source` + `timestamp` + `data-hash` + `verify` + `-----BEGIN` + `TRUSTED TIMESTAMP-----` + `-----END` + `mode=` + `fingerprint`

	out := make([][]byte, 0)

	for _, armour := range armoured {

		var b bytes.Buffer
		zw, err := flate.NewWriterDict(&b, flate.DefaultCompression, []byte(dict))
		if err != nil {
			return nil, err
		}

		io.Copy(zw, strings.NewReader(string(armour)))
		zw.Close()

		out = append(out, b.Bytes())
	}

	return out, nil
}

/* UncompressArmoured */
func UncompressArmoured(armoured [][]byte) ([][]byte, error) {

	const dict = `hash` + `source` + `timestamp` + `data-hash` + `verify` + `-----BEGIN` + `TRUSTED TIMESTAMP-----` + `-----END` + `mode=` + `fingerprint`

	out := make([][]byte, 0)

	for _, armour := range armoured {

		var b bytes.Buffer
		zw := flate.NewReaderDict(&b, []byte(dict))

		zw.Read(armour)
		zw.Close()

		out = append(out, b.Bytes())
	}

	return out, nil
}

/* TimestampArmourCompress */
func TimestampArmourCompress(datahash []byte, source string, prv *ecdsa.PrivateKey) ([]byte, error) {

	timestamp, err := NewTimestamp(datahash, source, prv)
	if err != nil {
		return nil, err
	}
	compressed, err := CompressArmoured(ArmourTimestamps([]*Timestamp{timestamp}))
	if err != nil {
		return nil, err
	}

	return compressed[0], nil
}


/* Fuzz see https://github.com/dvyukov/go-fuzz */
func Fuzz(data []byte) int {

	armours := make([][]byte,0)
	armours = append(armours,data)

	if _, err := DearmourTimestamps(armours); err != nil {
		return 0
	}
	return 1
}





